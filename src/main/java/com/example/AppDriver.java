package com.example;

import com.example.controller.GroceryItemController;
import com.example.controller.GroceryListController;
import com.example.dao.DBConnection;
import com.example.dao.GroceryItemDao;
import com.example.dao.GroceryListDao;
import com.example.service.GroceryItemService;
import com.example.service.GroceryListService;

import io.javalin.Javalin;

public class AppDriver {

	public static void main(String[] args) {
		// instance class from controller file
		GroceryListController glc = new GroceryListController(new GroceryListService(new GroceryListDao(new DBConnection())));
		GroceryItemController gic = new GroceryItemController(new GroceryItemService(new GroceryItemDao(new DBConnection())));
		
		
		// initializing the javalin
		Javalin app = Javalin.create(config -> {
			config.addStaticFiles("/frontend");
			config.enableCorsForAllOrigins();
		});
		
		// default port
		app.start(7005);
		
		/* APIs Methods for two operations: Grocery List and Grocery Item */
		
		// Grocery List 
		app.get("/grocery_list", glc.groceryListGet);
		app.post("/grocery_list", glc.groceryListPost);
		app.delete("/grocery_list/:costOfItem", glc.groceryDeleteList);
		app.delete("/grocery_list", glc.groceryDeleteAList);
		
		
		// Grocery Item
		app.post("/grocery_item/:costOfItem", gic.groceryInsertItem);
		app.delete("/grocery_list", gic.groceryDeleteItem);
		
		
		// if not found, return error
		app.exception(NullPointerException.class, (e, ctx) -> {
			e.printStackTrace();
			ctx.status(404);
		}).error(404, (ctx)->{
			ctx.result("404 NullPointerException");
		});

	}

}
