package com.example.controller;

import java.util.List;

import com.example.model.GroceryList;
import com.example.service.GroceryListService;

import io.javalin.http.Handler;

public class GroceryListController {
	
	private GroceryListService groceryListServ = new GroceryListService();
	
	public GroceryListController() {
		
	}
	
	public GroceryListController(GroceryListService groceryListServ) {
		this.groceryListServ = groceryListServ;
	}
	
	// find all grocery list
	public final Handler groceryListGet = (ctx) -> {
		List<GroceryList> list = groceryListServ.getAllGrocery();
		ctx.status(200);
		ctx.json(list);
	};
	
	// insert a new grocery list
	public final Handler groceryListPost = (ctx) -> {
		int id = 0;
		String nameList = "";
		boolean success = groceryListServ.insertGroceryList(id, nameList);
		if(success) {
			ctx.status(200);
		}else {
			ctx.status(404);
		}
	};
	
	// delete an item from grocery 
	public final Handler groceryDeleteList = (ctx) -> {
		int id = 0;
		String name = "";
		id = Integer.parseInt(ctx.pathParam("list_id"));
		name = ctx.pathParam("nameOfList");
		boolean success = groceryListServ.removeGroceryList(id, name);
		if(success) {
			ctx.status(200);
		}else {
			ctx.status(404);
		}
	};
	
	// delete a list from grocery
	public final Handler groceryDeleteAList = (ctx) -> {	
		int id = 0;
		String name = "";
		id = Integer.parseInt(ctx.pathParam("costOfItem"));
		name = ctx.pathParam("category");
		boolean success = groceryListServ.removeAGroceryList(id, name);
		if(success) {
			ctx.status(200);
		}else {
			ctx.status(404);
		}
	};
}
