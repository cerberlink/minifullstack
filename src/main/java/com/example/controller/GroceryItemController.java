package com.example.controller;

import com.example.service.GroceryItemService;

import io.javalin.http.Handler;

public class GroceryItemController {
	
	private GroceryItemService groceryItemServ = new GroceryItemService();
	
	public GroceryItemController() {
		
	}
	
	public GroceryItemController(GroceryItemService groceryItemServ) {
		super();
		this.groceryItemServ = groceryItemServ;
	}
	
	// insert item into the grocery
	public final Handler groceryInsertItem = (ctx) -> {
		int id = 0;
		String list = "";
		boolean success = groceryItemServ.insertGroceryItem(id, list);
		if(success) {
			ctx.status(200);
		} else {
			ctx.status(404);
		}
	};
	
	// delete an item 
	public final Handler groceryDeleteItem = (ctx) -> {
		int id = 0;
		String list = "";
		
		id = Integer.parseInt(ctx.pathParam("costOfItem"));
		list = ctx.pathParam("category");
		boolean success = groceryItemServ.deleteGroceryItem(id, list);
		if(success) {
			ctx.status(200);
		}else {
			ctx.status(404);
		}
	};
}
