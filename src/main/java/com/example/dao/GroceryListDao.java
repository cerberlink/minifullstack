package com.example.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.model.GroceryList;

public class GroceryListDao {
	
	private DBConnection dbc = new DBConnection();
	
	public GroceryListDao() {
		dbc = new DBConnection();
	}
	
	public GroceryListDao(DBConnection dbc) {
		super();
		this.dbc = dbc;
	}
	
	// get all grocery list
	public List<GroceryList> getAllGroceryList(){
		List<GroceryList> list = new ArrayList<GroceryList>();
		try(Connection con = dbc.getDBConnection()){
			String sql = "SELECT * FROM grocery_list";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				list.add(new GroceryList(rs.getInt(1), rs.getString(2)));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	// insert the new grocery list
	public boolean insertGroceryList(int id, String nameList) {
		int insert = 0;
		try(Connection con = dbc.getDBConnection()){
			String sql = "INSERT INTO grocery_list VALUES (%d, %d)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ps.setString(2, nameList);
			insert = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(insert == 1) {
			return true;
		}else {
			return false;
		}
	}
	
	// delete the grocery list
	public boolean deleteGroceryList(int id, String nameList) {
		int delete = 0;
		try(Connection con = dbc.getDBConnection()){
			String sql = "DELETE FROM grocery_list WHERE list_id = ?";
			PreparedStatement ps = con.prepareCall(sql);
			ps.setInt(1, id);
			ps.setString(1, nameList);
			delete = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(delete == 1) {
			return true;
		}else {
			return false;
		}
	}
	
	/*
	 * DANGEROUS -> it will may lose the data from database
	 */
	// delete a grocery list
	public boolean deleteAGroceryList(int id, String nameList) {
		int delete = 0;
		try(Connection con = dbc.getDBConnection()){
			String sql = "DELETE FROM grocery_list";
			PreparedStatement ps = con.prepareCall(sql);
			ps.setInt(1, id);
			ps.setString(1, nameList);
			delete = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(delete == 1) {
			return true;
		}else {
			return false;
		}
	}

}
