package com.example.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	private String url = "jdbc:mariadb://senpai.ceucrzu5qljz.us-west-1.rds.amazonaws.com:3306/grocerydb";
	private String username = "groceryUsers";
	private String password = "mypassword";
	
	public Connection getDBConnection() throws SQLException{
		return DriverManager.getConnection(url, username, password);
	}
}
