package com.example.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class GroceryItemDao {
	
	private DBConnection dbc = new DBConnection();
	
	public GroceryItemDao() {
		dbc = new DBConnection();
	}
	
	public GroceryItemDao(DBConnection dbc) {
		super();
		this.dbc = dbc;
	}
	
	// insert
	public boolean insertGroceryItem(int costItem, String category) {
		int insert = 0;
		try(Connection con = dbc.getDBConnection()){
			String sql = "INSERT INTO grocery_item VALUES (%d, %d)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, costItem);
			ps.setString(2, category);
			insert = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(insert == 1) {
			return true;
		}else {
			return false;
		}
	}
	
	// delete 
	public boolean deleteItem(int costItem, String category) {
		int delete = 0;
		try(Connection con = dbc.getDBConnection()){
			String sql = "DELETE FROM grocery_item WHERE costOfItem = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, costItem);
			ps.setString(2, category);
			delete = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(delete == 1) {
			return true;
		} else {
			return false;
		}
	}

}
