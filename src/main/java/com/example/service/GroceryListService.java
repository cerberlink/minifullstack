package com.example.service;

import java.util.List;

import com.example.dao.GroceryListDao;
import com.example.model.GroceryList;

public class GroceryListService {

	private GroceryListDao groceryListDao = new GroceryListDao();
	
	public GroceryListService() {
		
	}
	
	public GroceryListService(GroceryListDao groceryListDao) {
		super();
		this.groceryListDao = groceryListDao;
	}
	
	public List<GroceryList> getAllGrocery(){
		return groceryListDao.getAllGroceryList();
	}
	
	public boolean insertGroceryList(int id, String nameList) {
		return groceryListDao.insertGroceryList(id, nameList);
	}
	
	public boolean removeGroceryList(int id, String nameList) {
		return groceryListDao.deleteGroceryList(id, nameList);
	}
	
	public boolean removeAGroceryList(int id, String nameList) {
		return groceryListDao.deleteAGroceryList(id, nameList);
	}
}
