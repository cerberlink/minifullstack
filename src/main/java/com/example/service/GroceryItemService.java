package com.example.service;

import com.example.dao.GroceryItemDao;

public class GroceryItemService {
	
	private GroceryItemDao groceryItemDao = new GroceryItemDao();
	
	public GroceryItemService() {
		
	}
	
	public GroceryItemService(GroceryItemDao groceryItemDao) {
		super();
		this.groceryItemDao = groceryItemDao;
	}
	
	public boolean insertGroceryItem(int id, String list) {
		return groceryItemDao.insertGroceryItem(id, list);
	}
	
	public boolean deleteGroceryItem(int id, String list) {
		return groceryItemDao.deleteItem(id, list);
	}
}
