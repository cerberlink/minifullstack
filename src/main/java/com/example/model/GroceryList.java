package com.example.model;

public class GroceryList {
	
	private int listId;
	private String nameOfList;
	
	public GroceryList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GroceryList(int listId, String nameOfList) {
		super();
		this.listId = listId;
		this.nameOfList = nameOfList;
	}
	
	public GroceryList(String nameOfList) {
		super();
		this.nameOfList = nameOfList;
	}

	public int getListId() {
		return listId;
	}

	public void setListId(int listId) {
		this.listId = listId;
	}

	public String getNameOfList() {
		return nameOfList;
	}

	public void setNameOfList(String nameOfList) {
		this.nameOfList = nameOfList;
	}

	@Override
	public String toString() {
		return "GroceryListModel [listId=" + listId + ", nameOfList=" + nameOfList + "]";
	}
	
	
}
