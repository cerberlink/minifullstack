package com.example.model;

public class GroceryItem {
	
	private int costItem;
	private String category;
	
	public GroceryItem() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GroceryItem(int costItem, String category) {
		super();
		this.costItem = costItem;
		this.category = category;
	}
	
	public GroceryItem(String category) {
		super();
		this.category = category;
	}
	
	
	public int getCostItem() {
		return costItem;
	}

	public void setCostItem(int costItem) {
		this.costItem = costItem;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "GroceryItemModel [costItem=" + costItem + ", category=" + category + "]";
	}
}
